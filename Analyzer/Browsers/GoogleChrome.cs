﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace Analyzer.Browsers
{
    class GoogleChrome
    {
        public static List<URL> URLs = new List<URL>();
        public static IEnumerable<URL> GetHistory()
        {
            // Get Current Users App Data
            string documentsFolder = Environment.GetFolderPath
            (Environment.SpecialFolder.ApplicationData);
            string[] tempstr = documentsFolder.Split('\\');
            string tempstr1 = "";
            documentsFolder += "\\Google\\Chrome\\User Data\\Default";
            if (tempstr[tempstr.Length - 1] != "Local")
            {
                for (int i = 0; i < tempstr.Length - 1; i++)
                {
                    tempstr1 += tempstr[i] + "\\";
                }
                documentsFolder = tempstr1 + "Local\\Google\\Chrome\\User Data\\Default";
            }


            // Check if directory exists
            if (Directory.Exists(documentsFolder))
            {
                return ExtractUserHistory(documentsFolder);

            }
            return null;
        }

        static List<URL> ExtractUserHistory(string folder)
        {
            List<URL> urls = new List<URL>();

            string dbPath = folder + "\\History";

            if (File.Exists(dbPath))
            {
                string target = @"C:\Temp\History";

                if (File.Exists(target))
                {
                    File.Delete(target);
                }

                File.Copy(dbPath, target);

                string cs = @"Data Source=" + target;
                string sql = "Select url , title, datetime(last_visit_time / 1000000 + (strftime('%s', '1601-01-01')), 'unixepoch') From urls";

                using (SQLiteConnection c = new SQLiteConnection(cs))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                urls.Add(new URL(rdr[0].ToString(), rdr[1].ToString(), rdr[2].ToString(), "Google Chrome"));
                            }
                        }
                    }
                }
            }
            return urls;
        }
    }
    
}
