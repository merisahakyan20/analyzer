﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace Analyzer.Browsers
{
    public class Firefox
    {
        public List<URL> URLs { get; set; }
        public static IEnumerable<URL> GetHistory()
        {
            // Get Current Users App Data
            string documentsFolder = Environment.GetFolderPath
                             (Environment.SpecialFolder.ApplicationData);

            documentsFolder += "\\Mozilla\\Firefox\\Profiles\\";


            if (Directory.Exists(documentsFolder))
            {
                foreach (string folder in Directory.GetDirectories
                                                   (documentsFolder))
                {
                    return ExtractUserHistory(folder);
                }
            }
            return null;
        }



        static List<URL> ExtractUserHistory(string folder)
        {
            List<URL> urls = new List<URL>();

            string dbPath = folder + "\\places.sqlite";

            if (File.Exists(dbPath))
            {
                string target = @"C:\Temp\History";

                if (File.Exists(target))
                {
                    File.Delete(target);
                }

                File.Copy(dbPath, target);
                string cs = @"Data Source=" + target;
                string sql = "Select url , title, datetime(last_visit_date / 1000000 + (strftime('%s', '1601-01-01')), 'unixepoch') From moz_places";

                using (SQLiteConnection c = new SQLiteConnection(cs))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                urls.Add(new URL(rdr[0].ToString(), rdr[1].ToString(), rdr[2].ToString(), "Google Chrome"));
                            }
                        }
                    }
                }
            }
            return urls;
        }
    }
}
