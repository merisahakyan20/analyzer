﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analyzer.Browsers
{
    public class URL
    {
        string url;
        string title;
        string browser;
        string date;
        public URL(string url, string title, string date, string browser)
        {
            this.url = url;
            this.title = title;
            this.browser = browser;
            this.date = date;
        }

        public string getData()
        {
            return browser + " - " + title + " - " + url;
        }
    }
}
