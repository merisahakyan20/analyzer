﻿using Analyzer.Browsers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Analyzer
{
    class Program
    {
        static System.Collections.Specialized.StringCollection log = new System.Collections.Specialized.StringCollection();
        static string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        static void Main(string[] args)
        {
            var date = DateTime.Now;
            filePath = $"{filePath}\\{date.Year}.{date.Month}.{date.Day}-{date.Hour}_{date.Minute}_{date.Second}";
            Directory.CreateDirectory(filePath);

            WriteFiles();

            using (StreamWriter sw = new StreamWriter($"{filePath}\\files.txt", true))
            {
                sw.WriteLine(builder.ToString());
            }

            WriteBrowserHistories();
            Console.ReadKey();
        }

        static StringBuilder builder = new StringBuilder();
        public static void WriteFiles()
        {
            string[] drives = Environment.GetLogicalDrives();

            foreach (string dr in drives)
            {
                DriveInfo di = new DriveInfo(dr);

                if (!di.IsReady)
                {
                    Console.WriteLine("The drive {0} could not be read", di.Name);
                    continue;
                }
                DirectoryInfo rootDir = di.RootDirectory;
                builder.Append(rootDir.FullName);
                builder.AppendFormat("{0}", Environment.NewLine);
                ComputerInformations.ShowAllFoldersUnder(rootDir.FullName, 2,builder);
            }
            Console.WriteLine($"File directories saved in {filePath}\\files.txt");
        }
        public static void WriteBrowserHistories()
        {
            using (StreamWriter sw = new StreamWriter($"{filePath}\\chrome.txt", true))
            {
                //for chrome
                var chrome = GoogleChrome.GetHistory();
                if (chrome != null)
                    foreach (var item in chrome)
                    {
                        //Console.WriteLine(item.getData());
                        sw.WriteLine(item.getData());
                    }
            }

            using (StreamWriter sw = new StreamWriter($"{filePath}\\firefox.txt", true))
            {
                //for firefox
                var firefox = Firefox.GetHistory();
                if (firefox != null)
                    foreach (var item in firefox)
                    {
                        //Console.WriteLine(item.getData());
                        sw.WriteLine(item.getData());
                    }
            }

            using (StreamWriter sw = new StreamWriter($"{filePath}\\explorer.txt", true))
            {
                //for internet explorer
                var explorer = InternetExplorer.GetHistory();
                foreach (var item in explorer)
                {
                    Console.WriteLine(item.getData());
                    sw.WriteLine(item.getData());
                }
            }
            Console.WriteLine($"Browser histories saved in {filePath}");
        }
    }
}
