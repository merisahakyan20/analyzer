﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Analyzer
{
    public class ComputerInformations
    {
        static StringCollection log = new StringCollection();
        public static void ShowAllFoldersUnder(string path, int indent, StringBuilder sw)
        {
            if (!Directory.Exists(path))
                return;

            string[] dirs = Directory.GetDirectories(path).Where(s => !s.Contains("Recycle")).ToArray();
            foreach (string folder in dirs)
            {
                if (!Directory.Exists(folder) || folder.Contains("Documents and Settings"))
                {
                    log.Add("Access denied to folder : Documents and Settings");
                    continue;
                }

                sw.Append($"{new string(' ', indent)}{Path.GetFileName(folder)}");
                sw.AppendFormat("{0}", Environment.NewLine);
                string[] files;
                try
                {
                    files = Directory.GetFiles(folder);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
                foreach (string f in files)
                {
                    sw.Append($"{new string(' ', indent + 2)}{Path.GetFileName(f)}");
                    sw.AppendFormat("{0}", Environment.NewLine);
                }
                ShowAllFoldersUnder(folder, indent + 2, sw);
            }
        }

        public static void PrintAllfiles()
        {
            string[] drives = Environment.GetLogicalDrives();

            foreach (string dr in drives)
            {
                DriveInfo di = new DriveInfo(@"C:\Meri");

                if (!di.IsReady)
                {
                    Console.WriteLine("The drive {0} could not be read", di.Name);
                    continue;
                }
                DirectoryInfo rootDir = di.RootDirectory;
                WalkDirectoryTree(rootDir);
            }

            Console.WriteLine("Files with restricted access:");
            foreach (string s in log)
            {
                Console.WriteLine(s);
            }
        }
        static void WalkDirectoryTree(DirectoryInfo root)
        {
            Console.WriteLine(root.Name);
            FileInfo[] files = null;
            DirectoryInfo[] subDirs = null;

            try
            {
                files = root.GetFiles("*.*");
            }
            catch (UnauthorizedAccessException e)
            {
                log.Add(e.Message);
            }

            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            if (files != null)
            {
                foreach (FileInfo fi in files)
                {
                    Console.WriteLine(fi.Name);
                }

                subDirs = root.GetDirectories();

                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    WalkDirectoryTree(dirInfo);
                }
            }
        }

    }
}
